<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProgramController extends Controller
{
    //
    public function page1(){
    	return view('program/page-1');
    }
    public function page2(){
    	return view('program/page-2');
    }
    public function page3(){
    	return view('program/page-3');
    }
    public function page4(){
    	return view('program/page-4');
    }
    public function page5(){
    	return view('program/page-5');
    }
    public function page6(){
    	return view('program/page-6');
    }
    public function page7(){
    	return view('program/page-7');
    }
    public function page8(){
    	return view('program/page-8');
    }

    
}
