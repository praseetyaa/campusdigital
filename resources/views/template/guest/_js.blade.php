
	
	<!--====== Javascripts & Jquery ======-->
	<script src="{{ asset('templates/loans2go/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('templates/loans2go/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('templates/loans2go/js/jquery.slicknav.min.js') }}"></script>
	<script src="{{ asset('templates/loans2go/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('templates/loans2go/js/jquery-ui.min.js') }}"></script>
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script>
	  AOS.init();
	</script>
	<script src="{{ asset('templates/loans2go/js/main.js') }}"></script>
