<style>
    /* Sidebar */
    .sidebar-nav .has-arrow:after {top: 20px;}

    /* Table */
    #dataTable td {padding: .5rem;}
    #dataTable thead tr th {text-align: center;}
    #dataTable tbody tr td:first-child, #dataTable tbody tr td:last-child {text-align: center;}
    #dataTable td a.btn {width: 36px;}
    div.dataTables_wrapper div.dataTables_processing {background-color: #eeeeee;}

    /*baonk here*/
    :root{ 
        --color-1: #000E38;
        --color-1-light: #e4fff4;
        --color-2: #34495e; 
        --border-light: rgba(2555,255,255,.5); 
        --shadow: 0 .125rem .25rem rgba(0,0,0,.075);
        --transition: .25s ease;
        --transition-cubic: .5s cubic-bezier(0.65, 0.05, 0.36, 1);
        --font-family-sans-serif: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans","Liberation Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"!important;
    }
    .rounded-1{border-radius: .5em!important}
    .rounded-2{border-radius: 1em!important}
    .rounded-3{border-radius: 1.5em!important}
    .rounded-4{border-radius: 2em!important}
    .rounded-5{border-radius: 2.5em!important}
    .shadow{box-shadow: var(--shadow)!important}
    .text-decoration-none{text-decoration: none!important}

    .left-sidebar, .page-wrapper, .topbar .top-navbar .navbar-header, #navbarSupportedContent{transition: .25s ease}
    .alert, .card, .navbar-nav .dropdown-menu{border-radius: .5em}
</style>